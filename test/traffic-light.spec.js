const expect = require("chai").expect;
const { getTrafficHexColor } = require("../src/traffic-light");

describe("Traffic lights", () => {
    it("Test traffic light possibilities", () => {
        expect(true).to.equal(true);
        expect(getTrafficHexColor("RED")).to.equal("#FF0000");
        expect(getTrafficHexColor("GREEN")).to.equal("#00FF00");
        expect(getTrafficHexColor("YELLOW")).to.equal("#FFFF00");
        expect(getTrafficHexColor("non-color")).to.equal(undefined);
    });
});