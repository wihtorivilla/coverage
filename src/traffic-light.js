// src/traffic-light.js
const getTrafficHexColor = (word) => {
    if (word == "GREEN") {
        return "#00FF00";
    } else if (word == "YELLOW") {
        return "#FFFF00";
    } else if (word == "RED") {
        return "#FF0000";
    } else {
        return undefined;
    };
};

module.exports = { getTrafficHexColor };