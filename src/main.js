// main.js
const trafficLights = require("./traffic-light");

if (trafficLights.getTrafficHexColor("GREEN") === "#00FF00") {
    console.log("Green");
}
if (trafficLights.getTrafficHexColor("YELLOW") === "#FFFF00") {
    console.log("Yellow");
}
if (trafficLights.getTrafficHexColor("RED") === "#FF0000") {
    console.log("Red");
}
if (trafficLights.getTrafficHexColor("what") === undefined) {
    console.log("Not a traffic light color");
}